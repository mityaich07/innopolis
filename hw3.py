import re
import json
import requests
import aiohttp
import asyncio
import time


def read_cities(filename: str) -> list:
    """Функция для считывания названий городов из заданного файла

    Args:
        filename (str): путь к файлу

    Returns:
        list:  список извлеченных городов
    """    ''''''
    cities = []
    with open(filename, 'r', encoding="utf-8") as f:
        for line in f:
            result = re.search(r'[A-Za-z]+\s?[A-Za-z]*', line)
            if result:
                cities.append(result.group(0).strip())
    return cities


class WeatherParams:
    def __init__(self, request_dict: dict):
        """На вход получеам get-запрос в формате словаря, извлекаем все вложенные значения,
         и сохраняем как атрибуты объекта погоды
        """        
        for key, value in request_dict.items():
            if isinstance(value, dict):
                for nested_key, nested_value in value.items():
                    self.__setattr__(f'{key}_{nested_key}', nested_value)
            elif isinstance(value, list):
                list_values = value.copy()
                for value in list_values:
                    if isinstance(value, dict):
                        for nested_key, nested_value in value.items():
                            self.__setattr__(f'{key}_{nested_key}', nested_value)
                            
            else:
                self.__setattr__(key, value)
        
        
                
    def __repr__(self):
        return 'class WeatherParams with parameter: ' + ' '.join([f'{key} = {value},' for key, value in self.__dict__.items()])
    
    def save_to_file(self, path_file):
        '''Сохраняем результаты в файл'''
        with open(path_file, 'a', encoding='utf-8') as f:
            f.write(';'.join([f'{str(k)} : {str(v)}' for k,v in self.__dict__.items()]) + '\n')
        
                            



start_time = time.time()

async def main():
    openweatherAPI = '0fb610dab7456bc44dbdde2ddba9be71'
    cities = read_cities('cities.txt')*10

    async with aiohttp.ClientSession() as session:
        for city in cities:    
            # print(city)

            request_link = f'https://api.openweathermap.org/data/2.5/weather?q={city}&appid={openweatherAPI}&units=metric'
            async with session.get(request_link) as resp:
                r = await resp.json()
                weather_object = WeatherParams(r)
                weather_object.save_to_file('35.txt')


asyncio.run(main())
print("Скорость асинхронной обработки 100 городов--- %s seconds ---" % (time.time() - start_time))
# Скорость обработки 100 городов (10 городов запросили 10 раз) составила 5.24 seconds. Для примера рассмотрим синхронный запрос




start_time = time.time()

def main():
    openweatherAPI = '0fb610dab7456bc44dbdde2ddba9be71'
    cities = read_cities('cities.txt')*10
    
    for city in cities:    
        # print(city)
        openweatherAPI = '0fb610dab7456bc44dbdde2ddba9be71'
        request_link = f'https://api.openweathermap.org/data/2.5/weather?q={city}&appid={openweatherAPI}&units=metric'
        request_dict = requests.get(request_link)
        request_dict=json.loads(request_dict.text)


        weather_object = WeatherParams(request_dict)
        weather_object.save_to_file('sync_35.txt')
main()
print("Скорость синхронной обработки 100 городов--- %s seconds ---" % (time.time() - start_time))
# Скорость асинхронной обработки составила 24,4 seconds. 